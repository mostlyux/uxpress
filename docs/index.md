---
home: true
footer: MIT Licensed | Copyright © 2018-present
layout: Layout
---

<hero>
    <img :src="$withBase('/hero.png')" alt="GitLab + VuePress">
</hero>

# Mostly UX
## Looks like it works :)


### But what about vue components...do they still work?

[Let's find out](/comptest.html)
